export interface IPublicacion{
  titulo:string;
  fecha:Date;
  descripcion:string;
}