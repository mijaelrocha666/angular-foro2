import { Component, OnInit } from '@angular/core';
import { IPublicacion } from 'src/app/models/publicacion.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PublicacionService } from 'src/app/services/publicacion.service';
@Component({
  selector: 'app-publicaciones',
  templateUrl: './publicaciones.component.html',
  styleUrls: ['./publicaciones.component.css']
})
export class PublicacionesComponent implements OnInit {

  constructor(private _publicacionService:PublicacionService) { }
  posts:IPublicacion[]=[];
  ngOnInit(): void {
    this.posts=this._publicacionService.obtenerPost();
  }


}
